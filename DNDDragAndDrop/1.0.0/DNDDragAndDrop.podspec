Pod::Spec.new do |s|
  s.name         = "DNDDragAndDrop"
  s.version      = "1.0.0"
  s.summary      = "A library that helps you support drag and drop operations in your iOS applications."

  s.description  = <<-DESC
                   TEMPORARY PODSPEC

                   Usage
                   =======

                   The main class to interact with is the DNDDragAndDropController. You provide the controller with two kinds of views to work with:
                   Drag Sources: These are the views that can be used to begin a dragging operation
                   Drop Targets: These are the views where you can drop something

                   Example
                   =======
                   There's a simple example project called OBDragDropTest which demonstrates the basic functionality of the library.

                   DESC

  s.homepage     = "https://bitbucket.org/foensi/ios-drag-and-drop"
  s.license      = 'MIT'
  s.author       = { "foensi" => "??@??.com" }
  s.platform     = :ios, '6.1'
  s.source       = { :git => "git@bitbucket.org:fushunpoon/ios-drag-and-drop.git", :tag => "1.0.0" }
  s.source_files  = 'iOS Library/Sources/**/*.{h,m}'
  s.exclude_files = 'Classes/Exclude'

  s.public_header_files = 'iOS Library/Sources/**/*.h'
  s.prefix_header_file = "iOS Library/Sources/Prefix.pch"
  s.requires_arc = true
end
