Pod::Spec.new do |s|
  s.name         = "CHTCollectionViewWaterfallLayout"
  s.version      = "0.6"
  s.summary      = "The waterfall (i.e., Pinterest-like) layout for UICollectionView."
  s.description  = <<-DESC
                   CHTCollectionViewWaterfallLayout is a subclass of UICollectionViewLayout, and it trys to imitate
                   UICollectionViewFlowLayout"s usage as much as possible.  This layout is inspired by Pinterest. 
                   It also is compatible with PSTCollectionView.
                   DESC
  s.homepage     = "https://github.com/chiahsien/CHTCollectionViewWaterfallLayout"
  s.screenshots  = "https://raw.github.com/chiahsien/CHTCollectionViewWaterfallLayout/master/Screenshots/3-columns.png", "https://raw.githubusercontent.com/chiahsien/CHTCollectionViewWaterfallLayout/develop/Screenshots/2-columns.png"
  s.license      = "MIT"
  s.author       = { "Nelson Tai" => "chiahsien" }
  s.platform     = :ios, "6.0"
  s.source       = { :git => "https://github.com/chiahsien/CHTCollectionViewWaterfallLayout.git", :tag => "0.6" }
  s.source_files  = "*.{h,m}"
  s.public_header_files = "*.h"
  s.requires_arc = true
end
