Pod::Spec.new do |s|
  s.name         = "AksSegmentedSliderControl"
  s.version      = "1.0.1"
  s.summary      = "Slider that lets you select discrete values."
  s.description  = <<-DESC
                   Slider that lets you select discrete values.
                   "An easy to use segmented slider control for iOS application."
                   DESC
  s.homepage     = "https://github.com/aryansbtloe/AksSegmentedSliderControl"
  s.license      =  'None specified'
  s.author       = { "Alok Kumar Singh" => "aryans00007@gmail.com" }
  s.platform     = :ios
  s.resources    = "AksSegmentedSliderControl/holder*.png"
  s.source       = { :git => "https://github.com/fatuhoku/AksSegmentedSliderControl.git", :tag => "1.0.1" }
  s.source_files  = 'AksSegmentedSliderControl/AksSegmentedSliderControl.{h,m}' #, 'Classes/**/*.{h,m}'
  s.public_header_files = 'AksSegmentedSliderControl/AKSSegmentedSliderControl.h'
end
